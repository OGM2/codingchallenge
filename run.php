<?php

use Challenge\Config;
use Challenge\Shape\Rectangle;
use Challenge\Shape\Square;
use Challenge\Shape\Circle;
use Challenge\Shape\Ellipse;

include_once 'config.php';
include_once 'input.php';

// load classes

Config::load();

// render shapes

$rectangle = new Rectangle($rectangle['x'], $rectangle['y'], $rectangle['width'], $rectangle['height']);
$rectangle->render();

$square = new Square($square['x'], $square['y'], $square['size']);
$square->render();

$ellipse = new Ellipse($ellipse['x'], $ellipse['y'], $ellipse['diameterH'], $ellipse['diameterV']);
$ellipse->render();

$circle = new Circle($circle['x'], $circle['y'], $circle['size']);
$circle->render();
