<?php

namespace Challenge;

class Config 
{
	public static function load()
	{
		foreach(glob('class/parent/*.php') as $parentClass)
		{
			include $parentClass;
		}

		foreach(glob('class/*.php') as $class)
		{
			include $class;
		}
	}
}
