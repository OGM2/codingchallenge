<?php

namespace Challenge\Shape;

use Challenge\Shape\Parent\Shape;

class Ellipse extends Shape
{

	public $diameterH;
	
	public $diameterV;
	
	public function __construct($x, $y, $diameterH, $diameterV)
	{
		parent::__construct($x, $y);
		
		$this->setDiameterH($diameterH);
		
		$this->setDiameterV($diameterV);
	}

	public function setDiameterH($diameterH)
	{
		$this->diameterH = $diameterH;
	}

	public function setDiameterV($diameterV)
	{
		$this->diameterV = $diameterV;
	}

	public function getDiameterH()
	{
		return $this->diameterH;
	}

	public function getDiameterV()
	{
		return $this->diameterV;
	}

	public function render()
	{
		echo 'Ellipse ('.$this->getX().','.$this->getY().') diameterH='.$this->getDiameterH().' diameterV='.$this->getDiameterV()."\n";
	}
}