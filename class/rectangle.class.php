<?php

namespace Challenge\Shape;

use Challenge\Shape\Parent\Shape;

class Rectangle extends Shape
{

	public $width;

	public $height;
	
	public function __construct($x, $y, $width, $height)
	{
		parent::__construct($x, $y);

		$this->setWidth($width);

		$this->setHeight($height);
	}

	public function setWidth($width)
	{
		$this->width = $width;
	}

	public function setHeight($height)
	{
		$this->height = $height;
	}

	public function getWidth()
	{
		return $this->width;
	}

	public function getHeight()
	{
		return $this->height;
	}

	public function render()
	{
		echo 'Rectangle ('.$this->getX().','.$this->getY().') width='.$this->getWidth().' height='.$this->getHeight()."\n";
	}
}