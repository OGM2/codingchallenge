<?php

namespace Challenge\Shape\Parent;

class Shape
{
	
	public $posX;
	
	public $posY;

	public function __construct($x, $y)
	{
		$this->setX($x);

		$this->setY($y);
	}

	public function setX($x)
	{
		$this->posX = $x;
	}

	public function setY($y)
	{
		$this->posY = $y;
	}

	public function getX()
	{
		return $this->posX;
	}

	public function getY()
	{
		return $this->posY;
	}

}