<?php

namespace Challenge\Shape;

use Challenge\Shape\Parent\Shape;

class Square extends Shape
{

	public $size;
	
	public function __construct($x, $y, $size)
	{
		parent::__construct($x, $y);
		
		$this->setSize($size);
	}

	public function setSize($size)
	{
		$this->size = $size;
	}

	public function getSize()
	{
		return $this->size;
	}

	public function render()
	{
		echo 'Square ('.$this->getX().','.$this->getY().') size='.$this->getSize()."\n";
	}

}