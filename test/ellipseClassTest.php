<?php

namespace Challenge\Test;

use Challenge\Shape\Ellipse;
use PHPUnit\Framework\TestCase;

class EllipseClassTest extends TestCase
{

    protected $x;
    
    protected $y;
    
    protected $diameterH;
    
    protected $diameterV;

    protected $ellipse;

    public function setUp()
    {
        include_once '../class/parent/shape.class.php';
        include_once '../class/ellipse.class.php';

        $this->x = 47;
        $this->y = 51;
        $this->diameterH = 69;
        $this->diameterV = 21;
        $this->ellipse = new Ellipse($this->x, $this->y, $this->diameterH, $this->diameterV);
    }

    public function testCoordinatesAndDiameterConstructor()
    {        
        $this->assertAttributeEquals($this->x, 'posX', $this->ellipse);
        $this->assertAttributeEquals($this->y, 'posY', $this->ellipse);
        $this->assertAttributeEquals($this->diameterH, 'diameterH', $this->ellipse);
        $this->assertAttributeEquals($this->diameterV, 'diameterV', $this->ellipse);
    }

    public function testGetDiameters()
    {
        $this->assertSame($this->diameterH, $this->ellipse->getDiameterH());
        $this->assertSame($this->diameterV, $this->ellipse->getDiameterV());
    }
   
}