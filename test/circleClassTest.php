<?php

namespace Challenge\Test;

use Challenge\Shape\Circle;
use PHPUnit\Framework\TestCase;

class CircleClassTest extends TestCase
{

    protected $x;
    
    protected $y;
    
    protected $size;

    protected $circle;

    public function setUp()
    {
        include_once '../class/parent/shape.class.php';
        include_once '../class/circle.class.php';

        $this->x = 21;
        $this->y = 69;
        $this->size = 74;
        $this->circle = new Circle($this->x, $this->y, $this->size);
    }

    public function testCoordinatesAndSizeConstructor()
    {        
        $this->assertAttributeEquals($this->x, 'posX', $this->circle);
        $this->assertAttributeEquals($this->y, 'posY', $this->circle);
        $this->assertAttributeEquals($this->size, 'size', $this->circle);
    }

    public function testGetSize()
    {
        $this->assertSame($this->size, $this->circle->getSize());
    }
   
}