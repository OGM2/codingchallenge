<?php

namespace Challenge\Test;

use Challenge\Shape\Square;
use PHPUnit\Framework\TestCase;

class SquareClassTest extends TestCase
{

    protected $x;
    
    protected $y;
    
    protected $size;

    protected $square;

    public function setUp()
    {
        include_once '../class/parent/shape.class.php';
        include_once '../class/square.class.php';

        $this->x = 78;
        $this->y = 32;
        $this->size = 21;
        $this->square = new Square($this->x, $this->y, $this->size);
    }

    public function testCoordinatesAndSizeConstructor()
    {        
        $this->assertAttributeEquals($this->x, 'posX', $this->square);
        $this->assertAttributeEquals($this->y, 'posY', $this->square);
        $this->assertAttributeEquals($this->size, 'size', $this->square);
    }

    public function testGetSize()
    {
        $this->assertSame($this->size, $this->square->getSize());
    }
   
}