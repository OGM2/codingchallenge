<?php

namespace Challenge\Test;

use Challenge\Shape\Rectangle;
use PHPUnit\Framework\TestCase;

class RectangleClassTest extends TestCase
{

    protected $x;
    
    protected $y;
    
    protected $width;

    protected $height;

    protected $rectangle;

    public function setUp()
    {
        include_once '../class/parent/shape.class.php';
        include_once '../class/rectangle.class.php';

        $this->x = 35;
        $this->y = 41;
        $this->width = 62;
        $this->height = 58;
        $this->rectangle = new Rectangle($this->x, $this->y, $this->width, $this->height);
    }

    public function testCoordinatesAndDimensionsConstructor()
    {        
        $this->assertAttributeEquals($this->x, 'posX', $this->rectangle);
        $this->assertAttributeEquals($this->y, 'posY', $this->rectangle);
        $this->assertAttributeEquals($this->width, 'width', $this->rectangle);
        $this->assertAttributeEquals($this->height, 'height', $this->rectangle);
    }

    public function testGetWidthAndHeight()
    {
        $this->assertSame($this->width, $this->rectangle->getWidth());
        $this->assertSame($this->height, $this->rectangle->getHeight());
    }
   
}