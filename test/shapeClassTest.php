<?php

namespace Challenge\Test;

use Challenge\Shape\Parent\Shape;
use PHPUnit\Framework\TestCase;

class ShapeClassTest extends TestCase
{

    protected $x;

    protected $y;

    protected $shape;

    public function setUp()
    {
        include_once '../class/parent/shape.class.php';

        $this->x = 20;
        $this->y = 54;
        $this->shape = new Shape($this->x, $this->y);
    }

    public function testCoordinatesConstructor()
    {
        $this->assertAttributeEquals($this->x, 'posX', $this->shape);
        $this->assertAttributeEquals($this->y, 'posY', $this->shape);
    }

    public function testGetXAndGetY()
    {        
        $this->assertSame($this->x, $this->shape->getX($this->x));
        $this->assertSame($this->y, $this->shape->getY($this->y));
    }
    
}