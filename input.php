<?php

// input values

$rectangle = array(
	'x' => 10,
	'y' => 10,
	'width' => 30,
	'height' => 40
);

$square = array(
	'x' => 15,
	'y' => 30,
	'size' => 35
);

$ellipse = array(
	'x' => 100,
	'y' => 150,
	'diameterH' => 300,
	'diameterV' => 200
);

$circle = array(
	'x' => 1,
	'y' => 1,
	'size' => 300
);